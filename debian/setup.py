#! /usr/bin/env python

from distutils.core import setup

setup(
    name="pygpu",
    description="embedded language for programming GPU using Python",
    author="Calle Lejdfors",
    author_email="calle.lejdfors@cs.lth.se",
    url="http://www.cs.lth.se/home/Calle_Lejdfors/pygpu/",
    version="0.2.0a-629",
    packages=["pygpu", "pygpu.backends",
        "pygpu.compiler", "pygpu.GPU", "pygpu.utils"]
)
